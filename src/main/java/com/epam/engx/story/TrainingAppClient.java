package com.epam.engx.story;

import java.util.function.Predicate;

import com.epam.engx.story.domain.Lesson;
import com.epam.engx.story.domain.TrainingProgram;
import com.epam.engx.story.domain.TrainingProviderBuilder;
import com.epam.engx.story.iterator.ProgramIteratorBuilder;

public class TrainingAppClient {
    public static void main(String[] args) {
        var trainingProvider = new TrainingProviderBuilder("programs.json").build();

        trainingProvider.programs().forEach(program -> {
            var iteratorBuilder = new ProgramIteratorBuilder(program);

            System.out.println("=== " + program.title() + " ===");
            showAllModules(iteratorBuilder, program);
            showLessons(iteratorBuilder, program);
            showLessonByKeyWords(iteratorBuilder, "Code");
            showLessonByDifficultyLevel(iteratorBuilder, Lesson.DifficultyLevel.MEDIUM);
            showLessonByKeywordAndDifficultyLevel(iteratorBuilder, "Code", Lesson.DifficultyLevel.MEDIUM);
        });
    }

    private static void showLessonByKeywordAndDifficultyLevel(ProgramIteratorBuilder programIteratorBuilder, String keyword,
                                                              Lesson.DifficultyLevel difficultyLevel) {
        System.out.printf("%n--- Lessons by keyword: '%s' and difficulty difficulty: '%s' ---%n", keyword, difficultyLevel);
        Predicate<Lesson> difficultyLevelFilter = lesson -> lesson.title().contains(keyword) &&
                lesson.difficulty() == difficultyLevel;
        var it = programIteratorBuilder.buildFilteringLessonIterator(difficultyLevelFilter);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    private static void showLessonByDifficultyLevel(ProgramIteratorBuilder programIteratorBuilder, Lesson.DifficultyLevel difficultyLevel) {
        System.out.printf("%n--- Lessons by difficulty difficulty: '%s' ---%n", difficultyLevel);
        Predicate<Lesson> difficultyLevelFilter = lesson -> lesson.difficulty() == difficultyLevel;
        var it = programIteratorBuilder.buildFilteringLessonIterator(difficultyLevelFilter);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    private static void showLessonByKeyWords(ProgramIteratorBuilder programIteratorBuilder, String keyWord) {
        System.out.printf("%n--- Lessons by key words: '%s' ---%n", keyWord);
        Predicate<Lesson> keywordFilter = lesson -> lesson.title().contains(keyWord);
        var it = programIteratorBuilder.buildFilteringLessonIterator(keywordFilter);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    private static void showLessons(ProgramIteratorBuilder programIteratorBuilder, TrainingProgram program) {
        System.out.printf("%n--- Lessons for program: '%s' ---%n", program.title());
        var it = programIteratorBuilder.buildDefaultLessonIterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    private static void showAllModules(ProgramIteratorBuilder programIteratorBuilder, TrainingProgram program) {
        System.out.printf("%n--- Modules for program: '%s' ---%n", program.title());
        var it = programIteratorBuilder.buildDefaultModuleIterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
