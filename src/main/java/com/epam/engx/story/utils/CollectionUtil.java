package com.epam.engx.story.utils;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.epam.engx.story.domain.CourseModule;
import com.epam.engx.story.domain.Lesson;

public final class CollectionUtil {
    private CollectionUtil() {
        throw new IllegalStateException("private constructor");
    }

    public static <C extends Collection<Lesson>> C createCollection(
            Collection<CourseModule> source,
            Supplier<C> supplier,
            Predicate<Lesson> filter) {

        return source.stream()
                .flatMap(m -> m.lessons().stream())
                .filter(filter)
                .collect(Collectors.toCollection(supplier));
    }
}
