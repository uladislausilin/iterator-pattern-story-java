package com.epam.engx.story.iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

import com.epam.engx.story.domain.CourseModule;
import com.epam.engx.story.domain.Lesson;
import com.epam.engx.story.utils.CollectionUtil;

public class LessonIterator implements ModuleIterator<Lesson> {
    private final Iterator<Lesson> iterator;

    public LessonIterator(Collection<CourseModule> modules) {
        iterator = CollectionUtil.createCollection(modules, ArrayList::new, anyNonEmpty()).iterator();
    }

    private static Predicate<Lesson> anyNonEmpty() {
        return Predicate.not(l -> l.title().isEmpty());
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Lesson next() {
        if (hasNext()) {
            return iterator.next();
        }
        return null;
    }
}