package com.epam.engx.story.iterator;

import java.util.function.Predicate;

import com.epam.engx.story.domain.CourseModule;
import com.epam.engx.story.domain.Lesson;
import com.epam.engx.story.domain.TrainingProgram;

public class ProgramIteratorBuilder {

    private final TrainingProgram program;

    public ProgramIteratorBuilder(TrainingProgram program) {
        this.program = program;
    }

    public ModuleIterator<CourseModule> buildDefaultModuleIterator() {
        return new DefaultModuleIterator(program.modules());
    }

    public ModuleIterator<Lesson> buildDefaultLessonIterator() {
        return new LessonIterator(program.modules());
    }

    public ModuleIterator<Lesson> buildFilteringLessonIterator(Predicate<Lesson> filter) {
        return new FilteringLessonIterator(program.modules(), filter);
    }
}
