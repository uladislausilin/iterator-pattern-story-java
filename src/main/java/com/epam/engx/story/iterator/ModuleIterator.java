package com.epam.engx.story.iterator;

public interface ModuleIterator<T> {
    boolean hasNext();

    T next();
}
