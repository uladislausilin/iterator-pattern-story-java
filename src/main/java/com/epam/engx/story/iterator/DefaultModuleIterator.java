package com.epam.engx.story.iterator;

import java.util.Collection;
import java.util.Iterator;

import com.epam.engx.story.domain.CourseModule;

public class DefaultModuleIterator implements ModuleIterator<CourseModule> {
    private final Iterator<CourseModule> iterator;

    public DefaultModuleIterator(Collection<CourseModule> modules) {
        this.iterator = modules.iterator();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public CourseModule next() {
        if (hasNext()) {
            return iterator.next();
        }
        return null;
    }
}

