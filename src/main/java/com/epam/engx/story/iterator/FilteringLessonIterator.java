package com.epam.engx.story.iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

import com.epam.engx.story.domain.CourseModule;
import com.epam.engx.story.domain.Lesson;
import com.epam.engx.story.utils.CollectionUtil;

public class FilteringLessonIterator implements ModuleIterator<Lesson> {
    private final Iterator<Lesson> iterator;

    public FilteringLessonIterator(Collection<CourseModule> modules, Predicate<Lesson> filter) {
        iterator = CollectionUtil.createCollection(modules, ArrayList::new, filter).iterator();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();

    }

    @Override
    public Lesson next() {
        if (hasNext()) {
            return iterator.next();
        }
        return null;
    }
}