package com.epam.engx.story.domain;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TrainingProviderBuilder {
    private final String json;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public TrainingProviderBuilder(String fileName) {
        try (var resourceAsStream = TrainingProviderBuilder.class.getClassLoader().getResourceAsStream(fileName)) {
            json = new String(Objects.requireNonNull(resourceAsStream).readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public TrainingProvider build() {
        try {
            return objectMapper.readValue(json, TrainingProvider.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
