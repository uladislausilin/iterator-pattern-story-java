package com.epam.engx.story.domain;

public record Lesson(String title, DifficultyLevel difficulty) {
    public enum DifficultyLevel {
        LOW, MEDIUM, HIGH
    }
}
