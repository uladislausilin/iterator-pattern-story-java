package com.epam.engx.story.domain;

import java.util.Collection;

public record TrainingProgram(String title, Collection<CourseModule> modules) {}
