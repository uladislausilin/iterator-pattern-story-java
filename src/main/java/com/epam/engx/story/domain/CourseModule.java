package com.epam.engx.story.domain;

import java.util.Collection;

public record CourseModule(String title, String description, Collection<Lesson> lessons) {}
