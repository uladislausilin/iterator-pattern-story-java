package com.epam.engx.story.domain;

import java.util.Collection;

public record TrainingProvider(Collection<TrainingProgram> programs) {}
